import 'package:flutter/material.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';
import 'package:charts_flutter/flutter.dart' as charts;

void main() => runApp(MaterialApp(home: ProgressPage(),debugShowCheckedModeBanner: false,));

class ProgressPage extends StatefulWidget {
  @override
  _ProgressPageState createState() => _ProgressPageState();
}

class _ProgressPageState extends State<ProgressPage> {
  @override
  Widget build(BuildContext context) {

    //Charts
    var numbers = [
      Charts("الفرق 32",50),
      Charts("المنفذ 42",85),
      Charts("المقرر 170",170),
    ];

    var series = [
      charts.Series(
        domainFn: (Charts lev, _) => lev.char,
        measureFn: (Charts lev, _) => lev.level,
        id: "Numbers",
        data: numbers,
    )];

    var chart = charts.BarChart(
      series,
    );

    return Scaffold(
      appBar: AppBar(backgroundColor: Colors.teal[600]),
      backgroundColor: Colors.blueGrey[50],
      endDrawer: Drawer(),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[

            //Student Container With Stars
            Container(
              height: 100,
              width: double.infinity,
              margin: EdgeInsets.fromLTRB(12, 12, 12, 0),
              color: Colors.white,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: <Widget>[
                      Text(
                        "حمزة حسين عبدالرحمن",
                        style: TextStyle(
                            fontSize: 22,
                            color: Colors.teal[600],
                            fontWeight: FontWeight.bold),
                      ),
                      Row(
                        children: <Widget>[
                          Icon(Icons.star, color: Colors.orangeAccent),
                          Icon(Icons.star, color: Colors.orangeAccent),
                          Icon(Icons.star, color: Colors.orangeAccent),
                          Icon(Icons.star, color: Colors.orangeAccent),
                          Icon(Icons.star),
                        ],
                      ),
                      SizedBox(height: 10)
                    ],
                  ),
                  CircleAvatar(
                    child: Icon(Icons.person, color: Colors.white),
                    backgroundColor: Colors.teal[600],
                    maxRadius: 27,
                  ),
                  SizedBox(width: 8)
                ],
              ),
            ),

            //Quran Progress Container
            Container(
                height: MediaQuery.of(context).size.height > 720 ? 285 : 270,
                width: double.infinity,
                color: Colors.white,
                alignment: Alignment.centerRight,
                margin: EdgeInsets.all(12),
                padding: EdgeInsets.all(8),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            Column(
                              children: <Widget>[
                                Row(
                                  children: <Widget>[
                                    Container(
                                      padding: EdgeInsets.only(top: 15),
                                      child: Text(
                                        "30",
                                        style: TextStyle(
                                            color: Colors.grey, fontSize: 20),
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(top: 8),
                                      child: Text(
                                        "/",
                                        style: TextStyle(
                                            color: Colors.grey, fontSize: 20),
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                            Column(
                              children: <Widget>[
                                Container(
                                  child: Text(
                                    "14",
                                    style: TextStyle(
                                        color: Colors.teal[600],
                                        fontSize: 28,
                                        fontWeight: FontWeight.w600),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                        Text(
                          "مستوى التقدم في ختم القرآن",
                          style: TextStyle(
                              fontWeight: FontWeight.w500, fontSize: 17),
                        ),
                      ],
                    ),
                    Text("%42",
                        style: TextStyle(
                            color: Colors.teal[600],
                            fontSize: 16,
                            fontWeight: FontWeight.w500)),

                    //Linear Progress Horizontal Chart
                    Transform.rotate(
                      angle: 3.14,
                      child: new LinearPercentIndicator(
                        width: MediaQuery.of(context).size.width - 50,
                        lineHeight: 20.0,
                        percent: 0.42,
                        backgroundColor: Colors.grey,
                        progressColor: Colors.teal[600],
                      ),
                    ),

                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            Text(
                              "170",
                              style: TextStyle(
                                  color: Colors.teal[600],
                                  fontSize: 16,
                                  fontWeight: FontWeight.w600),
                            ),
                            SizedBox(width: 5),
                            Text(
                              "المقرر",
                              style: TextStyle(fontSize: 16),
                            ),
                          ],
                        ),
                        Container(
                          height: 25,
                          width: 1,
                          color: Colors.black87,
                        ),
                        Row(
                          children: <Widget>[
                            Text(
                              "42",
                              style: TextStyle(
                                  color: Colors.teal[600],
                                  fontSize: 16,
                                  fontWeight: FontWeight.w600),
                            ),
                            SizedBox(width: 5),
                            Text(
                              "المنفذ",
                              style: TextStyle(fontSize: 16),
                            ),
                          ],
                        ),
                        Container(
                          height: 25,
                          width: 1,
                          color: Colors.black87,
                        ),
                        Row(
                          children: <Widget>[
                            Text(
                              "32",
                              style: TextStyle(
                                  color: Colors.teal[600],
                                  fontSize: 16,
                                  fontWeight: FontWeight.w600),
                            ),
                            SizedBox(width: 5),
                            Text(
                              "الفرق",
                              style: TextStyle(fontSize: 16),
                            ),
                          ],
                        ),
                      ],
                    ),

                    SizedBox(height: 10),

                    //الايام المتبقية وتاريخ الختم
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: <Widget>[
                        Container(
                          height: 30,
                          width: 90,
                          color: Colors.blueGrey[50],
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: <Widget>[
                              Text("يوم"),
                              Text(
                                "32",
                                style: TextStyle(color: Colors.teal[600]),
                              ),
                              SizedBox(width: 7),
                              Text("باقي"),
                              SizedBox(width: 5)
                            ],
                          ),
                        ),
                        Container(
                          height: 30,
                          width: 180,
                          color: Colors.blueGrey[50],
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: <Widget>[
                              Text(
                                "1442/2/25",
                                style: TextStyle(color: Colors.teal[600]),
                              ),
                              Text("تاريخ الختم"),
                            ],
                          ),
                        ),
                      ],
                    ),

                    SizedBox(height: 15),

                    //النقاط
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: <Widget>[
                            Row(
                              children: <Widget>[
                                Text(
                                  "3345",
                                  style: TextStyle(
                                      color: Colors.teal[600],
                                      fontSize: 18,
                                      fontWeight: FontWeight.w600),
                                ),
                                SizedBox(width: 5),
                                Text("عدد النقاط")
                              ],
                            ),
                            Row(
                              children: <Widget>[
                                Text(
                                  "1442/2/25",
                                  style: TextStyle(color: Colors.teal[600]),
                                ),
                                SizedBox(width: 3),
                                Text("نقطة في"),
                                SizedBox(width: 3),
                                Text(
                                  "34",
                                  style: TextStyle(
                                    color: Colors.teal[600],
                                  ),
                                ),
                                SizedBox(width: 3),
                                Text("تنتهي"),
                              ],
                            )
                          ],
                        ),
                        Padding(
                          padding: const EdgeInsets.only(
                              right: 8.0, top: 8.0, bottom: 8.0),
                          child: Icon(
                            Icons.monetization_on,
                            color: Colors.yellow,
                            size: 37,
                          ),
                        ),
                      ],
                    )
                  ],
                )),

            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Column(
                      children: <Widget>[
                        Text("اختر تاريخ التحليل"),
                        Container(
                          height: 0.75,
                          width: 100,
                          color: Colors.black87,
                        ),
                      ],
                    ),
                    SizedBox(width: 5),
                    Icon(Icons.calendar_today, color: Colors.teal[600]),
                  ],
                ),
                Row(
                  children: <Widget>[
                    Column(
                      children: <Widget>[
                        Text("اختر مسار التحليل"),
                        Container(
                          height: 0.75,
                          width: 100,
                          color: Colors.black87,
                        ),
                      ],
                    ),
                    SizedBox(width: 5),
                    Icon(Icons.web, color: Colors.teal[600]),
                  ],
                ),
              ],
            ),

            //Dates With Charts
            Container(
              height: 460,
              width: double.infinity,
              margin: EdgeInsets.all(12),
              color: Colors.white,
              child: Column(
                children: <Widget>[
                  SizedBox(height: 10),
                  //عنوان المسار
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      Container(
                        alignment: Alignment.centerRight,
                        padding: EdgeInsets.only(right: 5),
                        height: 30,
                        width: 200,
                        color: Colors.blueGrey[50],
                        child: Text(
                          "مسار خطة الحفظ الرسمي",
                          style: TextStyle(
                            color: Colors.teal[600],
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      ),
                      SizedBox(width: 15),
                      Text("عنوان المسار"),
                      SizedBox(width: 10)
                    ],
                  ),

                  SizedBox(height: 10),
                  //تاريخ البداية
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      Container(
                        alignment: Alignment.centerRight,
                        padding: EdgeInsets.only(right: 5),
                        height: 30,
                        width: 200,
                        color: Colors.blueGrey[50],
                        child: Text(
                          "1442/2/25",
                          style: TextStyle(
                            color: Colors.teal[600],
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      ),
                      SizedBox(width: 16),
                      Text("تاريخ البداية"),
                      SizedBox(width: 10)
                    ],
                  ),

                  SizedBox(height: 10),
                  //تاريخ النهاية
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      Container(
                        alignment: Alignment.centerRight,
                        padding: EdgeInsets.only(right: 5),
                        height: 30,
                        width: 200,
                        color: Colors.blueGrey[50],
                        child: Text(
                          "1442/2/25",
                          style: TextStyle(
                            color: Colors.teal[600],
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      ),
                      SizedBox(width: 19),
                      Text("تاربخ النهاية"),
                      SizedBox(width: 10)
                    ],
                  ),

                  SizedBox(height: 10),
                  //المدة
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      Container(
                        alignment: Alignment.centerRight,
                        padding: EdgeInsets.only(right: 5),
                        height: 30,
                        width: 200,
                        color: Colors.blueGrey[50],
                        child: Text(
                          "40",
                          style: TextStyle(
                            color: Colors.teal[600],
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      ),
                      SizedBox(width: MediaQuery.of(context).size.width > 530 ? 29.0 : 21.5),
                      Text("المدة"),
                      SizedBox(width: 43)
                    ],
                  ),

                  SizedBox(height: 10),

                  Stack(
                    alignment: Alignment.topLeft,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: SizedBox(height: 270,child: chart,),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 20,top: 3),
                        child: Text("متقدم جداََ",style: TextStyle(fontSize: 12),),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 23,top: 78),
                        child: Text("متقدم",style: TextStyle(fontSize: 12),),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 22,top: 156),
                        child: Text("متوسط",style: TextStyle(fontSize: 12),),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 22,top: 232),
                        child: Text("ضعيف",style: TextStyle(fontSize: 12),),
                      ),
                    ],
                  ),
                ],
              ),
            ),

            //Result Progress
            Container(
              height: MediaQuery.of(context).size.height > 720 ? 557: 530,
              width: double.infinity,
              margin: EdgeInsets.all(12),
              color: Colors.white,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: <Widget>[
                  SizedBox(height: 20),

                  //الحضور
                  Padding(
                    padding: const EdgeInsets.only(right: 8.0),
                    child: Text("الحضور",style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.w500,
                    ),),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(right: 8.0),
                    child: Transform.rotate(
                      angle: 3.14,
                      child: new LinearPercentIndicator(
                        width: MediaQuery.of(context).size.width - 50,
                        lineHeight: 20.0,
                        percent: 0.50,
                        backgroundColor: Colors.grey,
                        progressColor: Colors.orangeAccent,
                        center: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Transform.rotate(
                                angle: 3.14,
                                child: Text("50%",style: TextStyle(color: Colors.white),)),
                          ],
                        ),
                      ),
                    ),
                  ),

                  //الحفظ الرسمي
                  Padding(
                    padding: const EdgeInsets.only(right: 8.0),
                    child: Text("الحفظ الرسمي",style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.w500,
                    ),),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(right: 8.0),
                    child: Transform.rotate(
                      angle: 3.14,
                      child: new LinearPercentIndicator(
                        width: MediaQuery.of(context).size.width - 50,
                        lineHeight: 20.0,
                        percent: 0.68,
                        backgroundColor: Colors.grey,
                        progressColor: Colors.blue[400],
                        center: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Transform.rotate(
                                angle: 3.14,
                                child: Text("68%",style: TextStyle(color: Colors.white),)),
                          ],
                        ),
                      ),
                    ),
                  ),

                  //المراجعة الصغرى
                  Padding(
                    padding: const EdgeInsets.only(right: 8.0),
                    child: Text("المراجعة الصغرى",style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.w500,
                    ),),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(right: 8.0),
                    child: Transform.rotate(
                      angle: 3.14,
                      child: new LinearPercentIndicator(
                        width: MediaQuery.of(context).size.width - 50,
                        lineHeight: 20.0,
                        percent: 0.20,
                        backgroundColor: Colors.grey,
                        progressColor: Colors.red,
                        center: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Transform.rotate(
                                angle: 3.14,
                                child: Text("20%",style: TextStyle(color: Colors.white),)),
                          ],
                        ),
                      ),
                    ),
                  ),

                  //المراجعة الكبرى
                  Padding(
                    padding: const EdgeInsets.only(right: 8.0),
                    child: Text("المراجعة الكبرى",style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.w500,
                    ),),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(right: 8.0),
                    child: Transform.rotate(
                      angle: 3.14,
                      child: new LinearPercentIndicator(
                        width: MediaQuery.of(context).size.width - 50,
                        lineHeight: 20.0,
                        percent: 0.95,
                        backgroundColor: Colors.grey,
                        progressColor: Colors.teal[600],
                        center: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Transform.rotate(
                                angle: 3.14,
                                child: Text("95%",style: TextStyle(color: Colors.white),)),
                          ],
                        ),
                      ),
                    ),
                  ),

                  //المراجعة الطارئة
                  Padding(
                    padding: const EdgeInsets.only(right: 8.0),
                    child: Text("المراجعة الطارئة",style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.w500,
                    ),),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(right: 8.0),
                    child: Transform.rotate(
                      angle: 3.14,
                      child: new LinearPercentIndicator(
                        width: MediaQuery.of(context).size.width - 50,
                        lineHeight: 20.0,
                        percent: 0.88,
                        backgroundColor: Colors.grey,
                        progressColor: Colors.teal[600],
                        center: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Transform.rotate(
                                angle: 3.14,
                                child: Text("88%",style: TextStyle(color: Colors.white),)),
                          ],
                        ),
                      ),
                    ),
                  ),

                  //تحسين التلاوة
                  Padding(
                    padding: const EdgeInsets.only(right: 8.0),
                    child: Text("تحسين التلاوة",style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.w500,
                    ),),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(right: 8.0),
                    child: Transform.rotate(
                      angle: 3.14,
                      child: new LinearPercentIndicator(
                        width: MediaQuery.of(context).size.width - 50,
                        lineHeight: 20.0,
                        percent: 0.50,
                        backgroundColor: Colors.grey,
                        progressColor: Colors.orangeAccent,
                        center: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Transform.rotate(
                                angle: 3.14,
                                child: Text("50%",style: TextStyle(color: Colors.white),)),
                          ],
                        ),
                      ),
                    ),
                  ),

                  //قراءة الإجازة
                  Padding(
                    padding: const EdgeInsets.only(right: 8.0),
                    child: Text("قراءة الإجازة",style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.w500,
                    ),),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(right: 8.0),
                    child: Transform.rotate(
                      angle: 3.14,
                      child: new LinearPercentIndicator(
                        width: MediaQuery.of(context).size.width - 50,
                        lineHeight: 20.0,
                        percent: 0.50,
                        backgroundColor: Colors.grey,
                        progressColor: Colors.orangeAccent,
                        center: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Transform.rotate(
                                angle: 3.14,
                                child: Text("50%",style: TextStyle(color: Colors.white),)),
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),

            SizedBox(height: 10),

            //Levels
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Text("ضعيف",style: TextStyle(fontSize: 13)),
                    SizedBox(width: 5),
                    CircleAvatar(backgroundColor: Colors.red,maxRadius: 5),
                  ],
                ),
                Row(
                  children: <Widget>[
                    Text("متوسط",style: TextStyle(fontSize: 13)),
                    SizedBox(width: 5),
                    CircleAvatar(backgroundColor: Colors.orangeAccent,maxRadius: 5),
                  ],
                ),
                Row(
                  children: <Widget>[
                    Text("متقدم",style: TextStyle(fontSize: 13)),
                    SizedBox(width: 5),
                    CircleAvatar(backgroundColor: Colors.blue[400],maxRadius: 5),
                  ],
                ),
                Row(
                  children: <Widget>[
                    Text("متقدم جداََ",style: TextStyle(fontSize: 13)),
                    SizedBox(width: 5),
                    CircleAvatar(backgroundColor: Colors.teal[600],maxRadius: 5),
                  ],
                ),
              ],
            ),
            SizedBox(height: 25),
          ],
        ),
      ),
    );
  }
}


class Charts {
  final String char;
  final int level;

  Charts(this.char,this.level);
}
